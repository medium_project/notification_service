FROM golang:1.19.1-alpine3.16 as builder

WORKDIR /notification

COPY . .

RUN go build -o main cmd/main.go

FROM alpine:3.16

WORKDIR /notification

COPY --from=builder /notification/main .

CMD [ "/notification/main" ]

